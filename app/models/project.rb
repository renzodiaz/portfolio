# frozen_string_literal: true

class Project < ApplicationRecord
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings
  has_many :project_categories
  has_many :categories, through: :project_categories
  belongs_to :client

  mount_uploader :image, ProjectUploader

  def all_tags=(names)
    self.tags = names.split(",").map do |name|
      Tag.where(name: name.strip).first_or_create!
    end
  end

  def all_tags
    self.tags.map(&:name).join(", ")
  end

end
