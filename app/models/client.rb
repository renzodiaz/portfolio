class Client < ApplicationRecord
  has_many :projects
  validates_presence_of :name, :company, :email, :website
end
