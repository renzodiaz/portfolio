# frozen_string_literal: true

class ApplicationController < ActionController::API
  serialization_scope :view_context
  include JWTSessions::RailsAuthorization
  rescue_from JWTSessions::Errors::Unauthorized, with: :not_authorized

  def fallback_index_html
    render :file => 'public/index.html'
  end

  private

  def current_user
    @current_user ||= User.find(payload['user_id'])
  end

  def not_authorized
    render json: { error: 'Not authorized' }, status: :unauthorized
  end
end
