# frozen_string_literal: true

class Api::V1::ProjectsController < ApplicationController
  before_action :authorize_access_request!, except: %i[index show]
  before_action :set_project, only: [:show, :update, :destroy, :destroy_many]

  # GET /projects
  def index
    @projects = Project.preload(:tags)
    
    render json: @projects
  end

  # GET /projects/:id
  def show
    render json: @project
  end

  # POST /projects
  def create
    @client = Client.find(params[:client_id])
    @project = @client.projects.create!(project_params)

    if @project.save
      render json: @project, status: :created, location: @projects
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /projects/:id
  def update
    if @project.update(project_params)
      render json: @project
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  # DELETE /projects/:id
  def destroy
    @project.destroy
  end

  # DELETE /projects/:ids
  def destroy_many
    @project.destroy_all
  end

  private

  def set_project
    if params[:ids]
      @project = Project.where(id: params[:ids].split(','))
    else
      @project = Project.find(params[:id])
    end
  end

  def project_params
    params.permit(:title, :image, :link, :summary, :client_id, :all_tags)
  end
end
