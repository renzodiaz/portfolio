class Api::V1::ArticlesController < ApplicationController
  before_action :authorize_access_request!, except: [:show, :index]
  before_action :set_article, only: [:show, :update, :destroy]

  # GET /articles
  def index
    @articles = Article.all
    render json: @articles
  end

  # GET /articles/:id
  def show
    render json: @article
  end

  # POST /articles
  def create
    @articles = Article.create(article_params)

    if @articles.save
      render json: @articles, status: :created, location: @article
    else
      render json: @articles.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /articles/:id
  def update
    if @article.update(article_params.except(:id, :updated_at, :created_at))
      render json: @article
    else
      render json: @article.errors, status: :unprocessable_entity
    end
  end

  # DELETE /articles/:id
  def destroy
    @article.destroy
  end

  private

  def set_article
    @article = Article.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :body, :image, :status)
  end
end
