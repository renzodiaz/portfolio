class Api::V1::UsersController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_user, only: [:show]

  def show
    @user
  end

  private
    def set_user
      @user = User.find_by_username(params[:username])
    end
end
