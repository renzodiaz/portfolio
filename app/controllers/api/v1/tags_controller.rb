class Api::V1::TagsController < ApplicationController
  before_action :authorize_access_request!, except: [:show, :index]
  before_action :set_tag, only: [:show, :update, :destroy, :destroy_many]

  # GET /tags
  def index
    @tags = Tag.all
    render json: @tags
  end

  # GET /tags/:id
  def show
    render json: @tag
  end

  # POST /tags
  def create
    @tags = Tag.create(tag_params)

    if @tags.save
      render json: @tags, status: :created, location: @tag
    else
      render json: @tags.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tags/:id
  def update
    if @tag.update(tag_params)
      render json: @tag
    else
      render json: @tag.errors, status: :unprocessable_entity
    end
  end

  # DELETE /tags/:id
  def destroy
    @tag.destroy
  end

  def destroy_many
    @tag.destroy_all
  end

  private

  def set_tag
    if (params[:ids])
      @tag = Tag.where(id: params[:ids].split(','))
    else
      @tag = Tag.find(params[:id])
    end
  end

  def tag_params
    params.require(:tag).permit(:name)
  end
end
