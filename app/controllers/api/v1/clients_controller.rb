class Api::V1::ClientsController < ApplicationController
  before_action :authorize_access_request!, except: [:show, :index]
  before_action :set_client, only: [:show, :update, :destroy]

  # GET /clients
  def index
    @clients = Client.select(:id, :name, :company, :website, :email, :phone, :created_at)
    paginate json: @clients
  end

  # GET /clients/:id
  def show
    render json: @client
  end

  # POST /clients
  def create
    @clients = Client.create(client_params)

    if @clients.save
      render json: @clients, status: :created, location: @client
    else
      render json: @clients.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /clients/:id
  def update
    if @client.update(client_params.except(:id, :updated_at, :created_at))
      render json: @client
    else
      render json: @client.errors, status: :unprocessable_entity
    end
  end

  # DELETE /clients/:id
  def destroy
    @client.destroy
  end

  private

  def set_client
    @client = Client.find(params[:id])
  end

  def client_params
    params.require(:client).permit(:name, :company, :website, :phone, :email)
  end

end
