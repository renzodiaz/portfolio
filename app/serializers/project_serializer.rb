class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :title, :image, :link, :summary, :tags
  has_one :client
end
