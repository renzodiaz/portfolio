const URL_PATTERN = /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([/\w .-]*)*\/?$/i
const EMAIL_PATTERN = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
const FULLNAME_PATTERN = /^([\w]{3,})+\s+([\w\s]{3,})+$/i
const API_URL = process.env.NODE_ENV === 'production' ? 'https://renzo-portfolio-staging.herokuapp.com' : 'http://localhost:3001'

export {URL_PATTERN, EMAIL_PATTERN, FULLNAME_PATTERN, API_URL}

