import { of, Observable } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { map, mergeMap, catchError } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import {sweetAlert} from '../utils/alert'
import { 
  USER_LOGIN, 
  USER_LOGOUT,
  USER_SIGNED_IN,
} from '../actions/types'
import { 
  userLoginSuccess,
  userLogoutSuccess,
  requestUserFailure
} from '../actions/userActions'

const API_URL = process.env.NODE_ENV === 'production' ? 'https://renzo-portfolio-staging.herokuapp.com' : 'http://localhost:3001'

export const userLoginEpic = action$ => action$.pipe(
  ofType(USER_LOGIN),
  mergeMap((action) => 
    ajax({
    url: `${API_URL}/signin`,
    method: 'POST',
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
    },
    body: {
      email: action.payload.email,
      password: action.payload.password
    }
    }).pipe(
      map(user => { 
        if (user.status === 200) {
          localStorage.signedIn = true
          localStorage.csrf = user.response.csrf
        }
        return userLoginSuccess(user.response) 
      }),
      catchError(error => { 
        sweetAlert("error", error.message)
        return of(requestUserFailure(error.message))
      })
    )
  )
)

export const userLogoutEpic = action$ => action$.pipe(
  ofType(USER_LOGOUT),
  mergeMap(() => 
    ajax({ 
      url: `${API_URL}/logout`,
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-TOKEN': localStorage.csrf
      }
    })
    .pipe(
      map(res => {
        delete localStorage.signedIn
        delete localStorage.csrf
        return userLogoutSuccess()
      }),
      catchError( error => {
        if(error.status === 401) {
          delete localStorage.signedIn
          delete localStorage.csrf
        }
        return of(requestUserFailure(error.message))
      })
    )
  )
)

export const userSignedInEpic = action$ => action$.pipe(
  ofType(USER_SIGNED_IN),
  mergeMap((action) => ajax({
    url: `${API_URL}/api/v1/users/renzodiaz`,
    method: 'GET',
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json'
    }
  }).pipe(
    map((user) => {
      console.log("refresh", user)
      return userLoginSuccess(user.response)
    }),
    catchError(error => {
      if (error.status === 401) {
        console.log('retry', error)
      }
      return of(requestUserFailure(error.message)) 
    })
  ))
)
