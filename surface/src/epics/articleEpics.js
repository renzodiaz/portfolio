import { of } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { map, mergeMap, catchError } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import {sweetAlert} from '../utils/alert'
import { API_URL } from '../constants'
import { 
  FETCH_ARTICLES,
  FETCH_ARTICLE,
  CREATE_ARTICLE,
  UPDATE_ARTICLE,
  DESTROY_ARTICLE
} from '../actions/types'

import { 
  fetchArticlesSuccess, 
  requestArticleFailure
} from '../actions/articleActions'

export const fetchArticlesEpic = action$ => action$.pipe(
  ofType(FETCH_ARTICLES),
  mergeMap(() => ajax.getJSON(`${API_URL}/api/v1/articles`).pipe(
    map( articles => fetchArticlesSuccess(articles)),
    catchError(error => of(requestArticleFailure(error.message)))
  ))
)

export const fetchArticleEpic = action$ => action$.pipe(
  ofType(FETCH_ARTICLE),
  mergeMap(action => ajax.getJSON(`${API_URL}/api/v1/articles/${action.payload}`).pipe(
    map( article => fetchArticlesSuccess(article)),
    catchError(error => of(requestArticleFailure(error.message)))
  ))
)
