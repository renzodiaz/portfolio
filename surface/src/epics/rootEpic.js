import { combineEpics } from 'redux-observable'
import { 
  fetchClientsEpic,
  fetchClientEpic,
  createClientEpic,
  updateClientEpic,
  destroyClientEpic
} from './clientEpics'

import {
  fetchProjectsEpic,
  fetchProjectEpic,
  createProjectEpic,
  updateProjectEpic,
  destroyProjectEpic
} from './projectEpics'

import {
  fetchArticlesEpic
} from './articleEpics'

import {
  userLoginEpic,
  userLogoutEpic,
  userSignedInEpic
} from './userEpics'

const rootEpic = combineEpics(
  fetchClientsEpic,
  fetchClientEpic,
  createClientEpic,
  updateClientEpic,
  destroyClientEpic,
  userLoginEpic,
  userLogoutEpic,
  userSignedInEpic,
  fetchProjectsEpic,
  fetchProjectEpic,
  createProjectEpic,
  updateProjectEpic,
  destroyProjectEpic,
  fetchArticlesEpic
)
export default rootEpic
