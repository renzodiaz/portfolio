import { of } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { map, mergeMap, catchError } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import {sweetAlert} from '../utils/alert'
import { API_URL } from '../constants'
import { 
  FETCH_CLIENTS,
  FETCH_CLIENT,
  CREATE_CLIENT,
  UPDATE_CLIENT,
  DESTROY_CLIENT
} from '../actions/types'

import { 
  fetchClientsSuccess,
  fetchClientSuccess, 
  createClientSuccess,
  updateClientSuccess,
  destroyClientSuccess,
  requestClientFailure
} from '../actions/clientActions'

export const fetchClientsEpic = action$ => action$.pipe(
  ofType(FETCH_CLIENTS),
  mergeMap(() => ajax.getJSON(`${API_URL}/api/v1/clients`).pipe(
    map( clients => fetchClientsSuccess(clients)),
    catchError(error => of(requestClientFailure(error.message)))
  ))
)

export const fetchClientEpic = action$ => action$.pipe(
  ofType(FETCH_CLIENT),
  mergeMap(action => ajax.getJSON(`${API_URL}/api/v1/clients/${action.payload}`).pipe(
    map( client => fetchClientSuccess(client)),
    catchError(error => of(requestClientFailure(error.message)))
  ))
)

export const createClientEpic = action$ => action$.pipe(
  ofType(CREATE_CLIENT),
  mergeMap( action => ajax({
    url: `${API_URL}/api/v1/clients`,
    method: 'POST',
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
      'X-CSRF-TOKEN': localStorage.csrf
    },
    body: {
      client: action.payload
    }
  }).pipe(
    map(client => createClientSuccess(client.response)),
    catchError(error => of(requestClientFailure(error.message)))
  ))
)

export const updateClientEpic = action$ => action$.pipe(
  ofType(UPDATE_CLIENT),
  mergeMap( action => ajax({
    url: `${API_URL}/api/v1/clients/${action.payload.id}`,
    method: 'PATCH',
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
      'X-CSRF-TOKEN': localStorage.csrf
    },
    body: {
      client: action.payload
    }
  }).pipe(
    map(client => { 
      sweetAlert("success", "Client updated successfully!")
      return updateClientSuccess(client.response) 
    }),
    catchError(error => of(requestClientFailure(error.message)))
  ))
)

export const destroyClientEpic = action$ => action$.pipe(
  ofType(DESTROY_CLIENT),
  mergeMap((action) => ajax({
    url: `${API_URL}/api/v1/clients/${action.payload}`,
    method: 'DELETE',
    withCredentials: true,
    headers: {
      'Content-Type' : 'application/json',
      'X-CSRF-TOKEN' : localStorage.csrf 
    }
  }).pipe(
    map(() => { 
      return destroyClientSuccess(action.payload)
    }),
    catchError(error => of(requestClientFailure(error.message)))
  ))
)
