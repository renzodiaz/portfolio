import { of } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { map, mergeMap, catchError } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import {sweetAlert} from '../utils/alert'
import { API_URL } from '../constants'
import { 
  FETCH_PROJECTS,
  FETCH_PROJECT,
  CREATE_PROJECT,
  UPDATE_PROJECT,
  DESTROY_PROJECT
} from '../actions/types'

import { 
  fetchProjectsSuccess,
  fetchProjectSuccess,
  createProjectSuccess,
  updateProjectSuccess,
  destroyProjectSuccess,
  requestProjectFailure
} from '../actions/projectActions'

export const fetchProjectsEpic = action$ => action$.pipe(
  ofType(FETCH_PROJECTS),
  mergeMap(() => ajax.getJSON('/api/v1/projects').pipe(
    map( projects => fetchProjectsSuccess(projects)),
    catchError(error => of(requestProjectFailure(error.message)))
  ))
)

export const fetchProjectEpic = action$ => action$.pipe(
  ofType(FETCH_PROJECT),
  mergeMap(action => ajax.getJSON(`/api/v1/projects/${action.payload}`).pipe(
    map( project => fetchProjectSuccess(project)),
    catchError(error => of(requestProjectFailure(error.message)))
  ))
)

export const createProjectEpic = action$ => action$.pipe(
  ofType(CREATE_PROJECT),
  mergeMap( action => ajax({
    url: `${API_URL}/api/v1/projects`,
    method: 'POST',
    withCredentials: true,
    headers: {
      'X-CSRF-TOKEN': localStorage.csrf
    },
    body: action.payload  }).pipe(
    map(project => { 
      sweetAlert("success", "Project created successfully")
      return createProjectSuccess(project.response)
    }),
    catchError(error => of(requestProjectFailure(error.message)))
  ))
)

export const updateProjectEpic = action$ => action$.pipe(
  ofType(UPDATE_PROJECT),
  mergeMap(action => ajax({
    url: `${API_URL}/api/v1/projects/${action.payload.id}`,
    method: 'PATCH',
    withCredentials: true,
    headers: {
      'X-CSRF-TOKEN': localStorage.csrf
    },
    body: action.payload.data
  }).pipe(
    map(project => { 
      sweetAlert("success", "Project updated successfully")
      return updateProjectSuccess(project.response)
    }),
    catchError(error => of(requestProjectFailure(error.message)))
  ))
)

export const destroyProjectEpic = action$ => action$.pipe(
  ofType(DESTROY_PROJECT),
  mergeMap( action => ajax({
    url: `${API_URL}/api/v1/projects/${action.payload}`,
    method: 'DELETE',
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
      'X-CSRF-TOKEN': localStorage.csrf
    }
  }).pipe(
    map(() => { 
      return destroyProjectSuccess(action.payload)
    }),
    catchError(error => of(requestProjectFailure(error.message)))
  ))
)
