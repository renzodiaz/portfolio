import React from 'react'
import { connect } from 'react-redux'
import {
  HashRouter,
  Switch
} from 'react-router-dom'

import { PrivateRoute, PublicRoute } from './routes'

import './app.sass'
import DashboardPage from './views/dashboard-page'
import ProjectsPage from './views/projects-page'
import ClientsPage from './views/clients-page'
import TagsPage from './views/tags-page'
import LoginPage from './views/login-page'
import NotFoundPage from './views/not-found-page'
import ArticlesPage from './views/articles-page'

class App extends React.Component {
  render() {
    return(
      <HashRouter basename="/">
        <Switch>
          <PrivateRoute path="/dashboard" component={DashboardPage} />
          <PrivateRoute path="/projects" component={ProjectsPage} />
          <PrivateRoute path="/clients" component={ClientsPage} />
          <PrivateRoute path="/tags" component={TagsPage} />
          <PrivateRoute path="/Articles" component={ArticlesPage} />
          <PublicRoute exact restricted={false} path="/" component={LoginPage} />
					<PublicRoute restricted={false} path="*" component={NotFoundPage} />
        </Switch>
      </HashRouter>
    )
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.user.isAuthenticated
})

export default connect(mapStateToProps)(App)
