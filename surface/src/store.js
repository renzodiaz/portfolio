import { createStore, applyMiddleware, compose } from 'redux'
import { createEpicMiddleware } from 'redux-observable'
import thunk from 'redux-thunk'
import rootReducer from './reducers'
import rootEpic from './epics/rootEpic'
import { userSignedIn } from './actions/userActions'

const initialState = {}

const middleware = [thunk]
const epicMiddleware = createEpicMiddleware()

const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(...middleware, epicMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
)

epicMiddleware.run(rootEpic)
store.dispatch(userSignedIn())
export default store
