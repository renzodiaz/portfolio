import React from 'react'
import {
  Route,
  Redirect
} from 'react-router-dom'
import { checkSignedIn } from './utils/user'
import Layout from './views/layout'

const PrivateRoute = ({ component: Component, ...props }) => {
  return (
  <Route
   {...props}
    render= { innerProps => checkSignedIn() ? 
      <Layout><Component { ...innerProps } /></Layout>
      : 
      <Redirect to="/" />  
    }
    />
  )
}

const PublicRoute = ({ component: Component, restricted, ...props }) => {
  console.log(checkSignedIn())
  return (
  <Route
   {...props}
    render= { innerProps => !checkSignedIn() && !restricted ? 
      <Component { ...innerProps } />
      : 
      <Redirect to="/dashboard" />  
    }
    />
  )
}

export { PrivateRoute, PublicRoute }
