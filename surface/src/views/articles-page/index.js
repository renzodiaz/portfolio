import React from 'react'
import { Switch, Route, useRouteMatch  } from 'react-router-dom'

import Article from '../../components/Article'
import NewArticle from '../../components/Article/New'

const ArticlesPage = () => {
  
  let { url } = useRouteMatch()

  return(
    <React.Fragment>
    <Switch>
      <Route exact path={ url } component={ Article } /> 
      <Route exact path={`${ url }/new`} component={ NewArticle } /> 
    </Switch>
    </React.Fragment>
  )
}

export default ArticlesPage
