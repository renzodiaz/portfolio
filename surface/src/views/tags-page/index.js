import React from 'react'
import { Switch, Route, useRouteMatch  } from 'react-router-dom'

import Tag from '../../components/Tag'
import New from '../../components/Tag/New'
import Edit from '../../components/Tag/Edit'

const TagsPage = () => {
  
  let { url } = useRouteMatch()

  return(
    <React.Fragment>
    <Switch>
      <Route exact path={ url } component={ Tag } /> 
      <Route path={ `${url}/new` } component={ New } /> 
      <Route path={ `${url}/edit/:id` } component={ Edit } /> 
    </Switch>
    </React.Fragment>
  )
}

export default TagsPage
