import React from 'react'
import { Link } from 'react-router-dom'

const NotFoundPage = (props) => {
  return(
    <section className="hero is-large">
      <div className="hero-body">
        <div className="container">
          <div className="columns is-vcentered">
            <div className="column has-text-centered">
              <h1 className="title">404 Page Not Found</h1>
              <p className="subtitle">An unexpected error has occurred. Please contact the site owner.</p>
              <Link to="/" className="button is-primary">Home</Link>
            </div>
            <div className="column has-text-centered">
              <img src="https://www.eastfieldcollege.edu/PublishingImages/Pages/PageNotFoundError/404-robot.gif" alt="Not Found" />
            </div>
          </div>
        </div>
      </div>
    </section> 
  )
}

export default NotFoundPage
