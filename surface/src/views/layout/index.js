import React from 'react'

import Header from '../../components/Shared/Header'
import Sidebar from '../../components/Shared/Sidebar'

class Layout extends React.Component {
  render() {
    return(
      <React.Fragment>
        <Header />
        <div className="columns is-fullheight">
          <div className="column is-2 is-sidebar-menu is-hidden-mobile">
            <Sidebar />
          </div>
          <div className="column is-main-content">
            <div className="box">{ this.props.children  }</div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default Layout
