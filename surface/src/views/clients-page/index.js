import React from 'react'
import { Switch, Route, useRouteMatch  } from 'react-router-dom'

import Client from '../../components/Client'
import New from '../../components/Client/New'
import Edit from '../../components/Client/Edit'

const ClientsPage = () => {
  let { url } = useRouteMatch()
  return(
    <React.Fragment>
    <Switch>
      <Route exact path={ url } component={ Client } /> 
      <Route path={ `${url}/new` } component={ New } /> 
      <Route path={ `${url}/edit/:id` } component={ Edit } /> 
    </Switch>
    </React.Fragment>
  )
}

export default ClientsPage
