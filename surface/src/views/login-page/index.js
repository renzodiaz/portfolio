import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { withFormik, Form } from 'formik'
import * as yup from 'yup'

import { userLogin } from '../../actions/userActions'
import { Input } from  '../../components/Shared/Form'

const Login = (props) => {

  const { isSubmitting, values, isAuthenticated} = props

  if (isSubmitting) {
    props.userLogin(values)
  }

  if (isAuthenticated) {
    return <Redirect to="/dashboard"/>
  } else {
    return (
      <section className="hero is-primary is-fullheight">
        <div className="hero-body">
          <div className="container">
            <div className="columns is-centered">
              <div className="column is-5-tablet is-4-desktop is-3-widescreen">
                <Form className="box">
                  <h1 className="title is-size-4 has-text-black">Login</h1>
                  <div className="field">
                    <label className="label">Email</label>
                    <div className="control has-icons-left">
                      <Input 
                        type="email"
                        name="email"
                        placeholder="fake@domain.com"
                        icon="envelope"/>
                    </div>
                  </div>
                  <div className="field">
                    <label className="label">Password</label>
                    <div className="control has-icons-left">
                      <Input 
                        type="password" 
                        placeholder="*******"
                        name="password"
                        autoComplete="true"
                        icon="lock"/> 
                    </div>
                  </div>
                  <div className="field">
                    <button type="submit" disabled={isSubmitting} className="button is-success">
                      Login
                    </button>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
const formikConfig =  {
  mapPropsToValues: () => (
    {email: "", password: ""}
  ),
  handleSubmit: async (values, {props, setSubmitting}) => {
    setSubmitting()
  },
  validationSchema: () => {
    return yup.object().shape({
      email: yup
      .string()
      .required("Email is required")
      .email("Enter a valid email"),
      password: yup
      .string()
      .required("Password is required:")
      .min(8, "Password is too short - 8 chars min")
    })
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.user.isAuthenticated
})

export default withFormik(formikConfig)(connect(mapStateToProps, { userLogin })(Login))
