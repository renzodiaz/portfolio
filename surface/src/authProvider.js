import { plainAxiosInstance, secureAxiosInstance  } from './Auth'

export default {
  login: ({ username, password }) => {
    return plainAxiosInstance.post('/signin', {email: username, password: password})
      .then(response => {
        if (!response.data.csrf) {
          return
        }
        localStorage.csrf = response.data.csrf
        localStorage.signedIn = true
        return response
      })
      .catch(error => {
        this.error = (error.response && error.response.data && error.response.data.error) || ''
        delete localStorage.csrf
        delete localStorage.signedIn
      });
  },
  logout: () => {
    return secureAxiosInstance.delete('/signin')
      .then(response => {
        delete localStorage.csrf
        delete localStorage.signedIn
      })
      .catch(error => console.log(error, 'Cannot sign out'))
  },
  checkAuth: () => {
    return localStorage.signedIn ? Promise.resolve() : Promise.reject()
  },
  getPermissions: () => {
    return localStorage.signedIn ? Promise.resolve() : Promise.reject()
  }
}
