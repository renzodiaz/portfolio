import React from 'react'
import isEmpty from 'lodash/isEmpty'
import * as yup from 'yup'
import { withFormik, Form } from 'formik'
import { Input } from '../Shared/Form'
import { URL_PATTERN, FULLNAME_PATTERN } from '../../constants'

const INITIAL_VALUE = {
  name: "",
  company: "",
  email: "",
  website: "",
  phone: ""
}

const handleSubmit = async (data, props) => {
  try {
    const isEdit = isEmpty(props.client) 
    const clientData = isEdit ? data : {...props.client, ...data}
    await props.onSubmitAction(clientData)
    if (!isEdit)
      console.log("success", "Client saved")
  } catch (error) {
    console.log(error, "error")
  }
}


const FormClient = props => {
  const { isSubmitting } = props

  return (
    <Form>
      <div className="field">
        <label className="label">Full name <sup className="has-text-danger">*</sup></label>
        <Input
          name="name"
          placeholder="e.g. John Doe"
        />
      </div> 
      <div className="field">
        <label className="label">Company <sup className="has-text-danger">*</sup></label>
        <Input 
          name="company"
          placeholder="e.g. Google Corp"
        />
      </div>
      <div className="field">
        <label className="label">Email <sup className="has-text-danger">*</sup></label>
        <Input 
          name="email"
          type="email"
          placeholder="e.g. jh@fake.com"
        />
      </div>
      <div className="field">
        <label className="label">Website</label>
        <Input 
          name="website"
          type="url"
          placeholder="e.g. http://domain.com"
        />
      </div>
      <div className="field">
        <label className="label">Phone</label>
        <Input 
          name="phone"
          type="tel"
          placeholder="Phone number or cellphone"
        />
      </div>
      <div className="field is-grouped is-grouped-right">
        <div className="control">
          <button disabled={isSubmitting} type="submit" className={ `button is-success ${isSubmitting ? 'is-loading' :''}` }>Save</button>
        </div>
      </div>
    </Form>
  )
}

const formikConfig = {
  mapPropsToValues: ({ client = {}}) => {
    return {...INITIAL_VALUE, ...client
  }},
  handleSubmit: async (values, {props, resetForm, setSubmitting}) => {

    const {
      name,
      company,
      email,
      website,
      phone
    } = values

    const data = {
      name: name,
      company: company,
      email: email,
      website: website,
      phone: phone
    }

    await handleSubmit(data, props)

    setSubmitting()
    if(isEmpty(props.client)) {
      resetForm()
      props.history.push("/clients")
    }
  },
  validationSchema: () => {
    return yup.object().shape({
      name: yup
      .string()
      .required("Fullname is required")
      .matches(FULLNAME_PATTERN, "Please enter a valid fullname"),
      company: yup
      .string()
      .required("Company name is required"),
      email: yup
      .string()
      .email("Enter a valid email")
      .required(),
      website: yup
      .string()
      .matches(URL_PATTERN, "Enter a valid URL"),
      phone: yup
      .number()
    })
  }
}

export default withFormik(formikConfig)(FormClient)
