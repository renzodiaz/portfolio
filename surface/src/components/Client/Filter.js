import React from 'react'
import { Link } from 'react-router-dom'

const Filter = (props) => {
  return(
    <nav className="level">
      <div className="level-left">
        <p className="level-item"><strong><small>Show</small>&nbsp;&nbsp;</strong></p>
        <div className="level-item">
          <div className="select is-small">
            <select>
              <option>5</option>
              <option>10</option>
            </select>
          </div>
        </div>
      </div>
      <div className="level-right">
        <p className="level-item">
          <button className="button is-small">
            <span>Export</span>
            <span className="icon">
              <i className="fas fa-file-excel"></i>
            </span>
          </button>
        </p>
        <p className="level-item">
          <Link to="/clients/new" className="button is-small is-success">
            <span>New Client</span>
            <span className="icon">
              <i className="fas fa-plus"></i>
            </span>
          </Link>
        </p>
      </div>
    </nav>
  )
}

export default Filter
