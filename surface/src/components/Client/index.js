import React, {Fragment} from 'react'
import List from './List'
import Filter from './Filter'

const Client = (props) => {
  return (
    <Fragment>
      <h1 className="title is-size-4">Clients</h1>
      <Filter />
      <List />
    </Fragment>
  )
}

export default Client
