import React, { useEffect } from 'react'
import isEmpty from 'lodash/isEmpty'
import { connect } from 'react-redux'
import { fetchClient, updateClient } from '../../actions/clientActions'

import FormClient from './FormClient'

const Edit = ({client, fetchClient, clientClear, updateClient, match}) => {
  useEffect(()=>{
    fetchClient(match.params.id)
  }, [fetchClient, match.params.id])
  
  return (
    isEmpty(client) ? null :
    <div className="columns is-centered">
      <div className="column is-4">
        <h1 className="title is-size-4">Edit client</h1>
        <FormClient client={client} onSubmitAction={updateClient} />
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  client: state.clients.item
})

export default connect(mapStateToProps, {fetchClient, updateClient})(Edit)
