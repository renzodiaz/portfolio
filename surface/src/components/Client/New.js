import React from 'react'
import {connect} from 'react-redux'
import FormClient from './FormClient'
import { createClient } from '../../actions/clientActions'

const New = ({createClient, history}) => {
  return (
    <div className="columns is-centered">
      <div className="column is-4">
        <h1 className="title is-size-4">New client</h1>
        <FormClient onSubmitAction={createClient} history={history}/>
      </div>
    </div>
  )
}

export default connect(null, {createClient})(New)
