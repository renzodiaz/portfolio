import React, { useEffect } from 'react'
import Moment from 'react-moment'
import swal from 'sweetalert'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchClients, destroyClient } from '../../actions/clientActions'


const List = (props) => {
  const { clients, isLoading, fetchClients, destroyClient } = props
  
  useEffect(() => {
    fetchClients()
  }, [fetchClients])

  const clientItems = clients && clients.map(client =>(
    <tr key={client.id}>
      <td><strong>{client.name}</strong></td>
      <td>{client.company}</td>
      <td>{client.email}</td>
      <td>{client.phone}</td>
      <td>
        <a href={client.website} className="button is-small" target="blank" rel="no-opener no-follow">
          <span className="icon"><i className="fas fa-external-link-alt"></i></span>
          <span>Project link</span>
        </a>
      </td>
      <td><Moment format="LL">{client.created_at}</Moment></td>
      <td>
        <div className="buttons is-centered">
          <Link to={ `clients/edit/${client.id}`} className="button is-small is-info">
            <span>Edit</span>
            <span className="icon is-small">
              <i className="fas fa-edit"></i>
            </span>
          </Link>
          <button type="button" data-id={client.id} className="button is-small is-danger" onClick={() => {
            swal({
              text: `are you sure want to delete ${client.name}`,
              icon: "warning",
              dangerMode: true,
              buttons: true
            }).then((willDelete) => {
              if (!willDelete) {
                swal.close()
                return;
              }
              destroyClient(client.id)
            }) 
            }
          }>
            <span>Delete</span>
            <span className="icon is-small">
              <i className="fas fa-times"></i>
            </span>
          </button>
        </div>
      </td>
    </tr>
  ))

  return(
    isLoading ? <h1>Loading</h1> :
    <table className="table is-fullwidth is-striped is-hoverable">
      <thead>
        <tr>
          <th>Name</th>
          <th>Company</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Website</th>
          <th>Creation Date</th>
          <th className="has-text-centered">Actions</th>
        </tr>
      </thead>
      <tbody>
        {clientItems}
      </tbody>
    </table>
  )
}

const mapStateToProps = state => ({
  clients: state.clients.items,
  isLoading: state.clients.isLoading
})

export default connect(mapStateToProps, {fetchClients, destroyClient})(List)
