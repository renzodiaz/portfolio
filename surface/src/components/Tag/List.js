import React, { useEffect } from 'react'
import isEmpty from 'lodash/isEmpty'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchTags, destroyTag } from '../../actions/tagActions'

const List = ({ fetchTags, destroyTag, tags }) => {

  useEffect(()=> {
    fetchTags()
  }, [fetchTags])

  return (
    <>
    <h1 className="title is-size-4">Tags</h1>
    <nav className="level">
      <div className="level-left">
       <div className="level-item"></div> 
      </div>
      <div className="leve-right">
        <div className="level-item">
          <Link to="tags/new" className="button is-small is-success">
            <span>New tag</span>
            <span className="icon"><i className="fas fa-plus"></i></span>
          </Link>
        </div>
      </div>
    </nav>
    { isEmpty(tags) ? <h2>No data</h2> :
    <div className="table-container">
      <table className="table is-striped is-hoverable">
        <thead>
          <tr>
            <th>Name</th>
            <th className="has-text-centered">Actions</th>
          </tr>
        </thead>
        <tbody>
    { tags && tags.map((tag) => (
          <tr key={tag.id}>
            <td><span className="tag">{tag.name}</span></td>
            <td>
              <div className="buttons is-centered">
                <Link to={`tags/edit/${tag.id}`} className="button is-small is-info">
                  <span>Edit</span>
                  <span className="icon"><i className="fa fa-edit"></i></span>
                </Link>
                <button type="button" className="button is-small is-danger" onClick={() => destroyTag(tag.id)}>
                  <span>Delete</span>
                  <span className="icon"><i className="fa fa-times"></i></span>
                </button>
              </div>
            </td>
          </tr>
    )) }
        </tbody>
      </table>
    </div>}
    </>
  )
} 

const mapStateToProps = state => ({
  tags: state.tags.items
})

export default connect(mapStateToProps, {fetchTags, destroyTag})(List)
