import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { createTag, tagClear } from '../../actions/tagActions'
import FormTag from './FormTag'

const New = ({ createTag, tagClear }) => {
  useEffect(() => {
    return () => tagClear()
  }, [tagClear])

  return (
    <div className="columns is-centered">
      <div className="column is-4">
        <h1 className="title is-size-4">New Tag</h1>
          <FormTag onSubmitAction={createTag}/>
      </div>
    </div>
  )
}

export default connect(null, { createTag, tagClear})(New)
