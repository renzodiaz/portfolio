import React, { useEffect } from 'react'
import FormTag from './FormTag'
import isEmpty from 'lodash/isEmpty'
import { connect } from 'react-redux'
import { fetchTag, tagClear, updateTag } from '../../actions/tagActions'

const Edit = ({fetchTag, updateTag, tagClear, tag, match}) => {
  useEffect(()=> {
    fetchTag(match.params.id)
    return () => tagClear()
  }, [fetchTag, tagClear, match])

  return (
    isEmpty(tag) ? null :
    <div className="columns is-centered">
      <div className="column is-4">
        <h1 className="title">Edit tag</h1>
        <FormTag onSubmitAction={updateTag} tag={tag}/>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  tag: state.tags.item
})

export default connect(mapStateToProps, {fetchTag, updateTag, tagClear})(Edit)
