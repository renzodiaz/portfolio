import React from 'react'
import isEmpty from 'lodash/isEmpty'
import * as yup from 'yup'
import { withFormik, Form } from 'formik'
import { Input } from '../Shared/Form'

const INITIAL_STATE = {
  name: ""
}

const handleSubmit = async (data, props) => {
  try {
    const isEdit = !isEmpty(props.tag)
    const formData = isEdit ? {...props.tag, ...data} : data
    await props.onSubmitAction(formData)
  } catch(error) {
    console.log(error)
  }
}

const FormTag = props => {
  const {isSubmitting} = props

  return (
    <Form>
      <div className="field">
        <label className="label">Name</label>
        <Input name="name" placeholder="e.g #rails"/>
      </div>
      <div className="field">
        <button type="submit" disabled={isSubmitting}className="button is-success">
          Save
        </button>
      </div>
    </Form>
  )
}

const formikConfig = {
  mapPropsToValues: ({ tag = {} }) => {
    return {...INITIAL_STATE, ...tag}
  },
  handleSubmit: async(values, { props, resetForm, setSubmitting }) => {
    const { name } = values
    await handleSubmit({name: name}, props)
    setSubmitting()
    if (isEmpty(props.tag)) {
      resetForm()
      props.history.push("/tags")
    } 
  },
  validationSchema: () => {
    return yup.object().shape({
      name: yup
      .string()
      .required("Tag name is required")
      .matches(/#([a-z-]*)/, "Enter a valid tag")
    })
  }
}

export default withFormik(formikConfig)(FormTag)
