import React, {Fragment} from 'react'
import {Field, ErrorMessage} from 'formik'

const Error = ({children}) => (
  <p className="help is-danger">{children}</p>
)

const Icon = ({icon}) => (
  <span className="icon is-small is-left">
    <i className={`fas fa-${icon}`} />
  </span>
)

export const Input = props => {
  const {name, icon = ""} = props

  return(
    <Fragment>
      <div className="control">
        <Field className="input" type="text" {...props} />
        { icon ? 
        <Icon icon={icon}/> :
          null }
      </div>
      <ErrorMessage name={name} component={Error} />
    </Fragment>
  )
}

export const Select = props => {
  const { name, children, placeholder } = props
  return(
    <React.Fragment>
      <div className="select">
        <Field as="select" {...props}>
          <option value="">{placeholder}</option>
          {children}
        </Field>
      </div>
      <ErrorMessage name={name} component={Error} />
    </React.Fragment>
  )
}

export const MultiSelect = props => {
  const { name, children, placeholder } = props
  return(
    <React.Fragment>
      <div className="select is-multiple">
        <select multiple {...props}>
          <option value="">{placeholder}</option>
          {children}
        </select>
      </div>
      <ErrorMessage name={name} component={Error} />
    </React.Fragment>
  )
}


export const File = props => {
  const {name} = props
	return(
    <React.Fragment>
      <div className="file has-name is-fullwidth">
        <label className="file-label">
          <input className="file-input" type="file" {...props} />
          <span className="file-cta">
            <span className="file-icon">
              <i className="fas fa-upload" />
            </span>
            <span className="file-label">
              Choose a file…
            </span>
          </span>
          <span id="filename" className="file-name"></span>
        </label>
      </div>
      <ErrorMessage name={name} component={Error} />
    </React.Fragment>
	)
}
