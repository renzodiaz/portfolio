import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { userLogout } from '../../../actions/userActions'
import Logo from './logo.svg'

const Header = props => {
  const {userLogout} = props

  return (
    <nav className="navbar">
      <div className="navbar-brand">
        <Link to="/" className="navbar-item">
          <img src={Logo} alt="Renzo Diaz" width={112} height={28} />
        </Link>
        <div className="navbar-burger burger" data-target="navMenubd-example">
          <span />
          <span />
          <span />
        </div>
      </div>
      <div id="navMenubd-example" className="navbar-menu">
        <div className="navbar-end">
          <div className="navbar-item has-dropdown is-hoverable">
            <div className="navbar-link">
              Username
            </div>
            <div className="navbar-dropdown is-right">
              <a className="navbar-item " href="http://bulma.io/documentation/elements/box/">
                Profile
              </a>
              <hr className="navbar-divider" />
              <button className="navbar-item" onClick={ () => userLogout()}>
                Logout
              </button>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default connect(null, {userLogout})(Header)
