import React from 'react'
import {
  NavLink
} from 'react-router-dom'

const Sidebar = () => {
  return(
    <aside className="menu">
      <p className="menu-label">
        General
      </p>
      <ul className="menu-list">
        <li><NavLink exact to="/" activeClassName="is-active">Dashboard</NavLink></li>
      </ul>
      <p className="menu-label">
        Administration
      </p>
      <ul className="menu-list">
        <li>
          <NavLink to='/projects' activeClassName="is-active">Projects</NavLink>
        </li>
        <li>
          <NavLink to='/clients' activeClassName="is-active">Clients</NavLink>
        </li>
        <li>
          <NavLink to="/tags" activeClassName="is-active">Tags</NavLink>
        </li>
        <li><NavLink to="/articles" activeClassName="is-active">Articles</NavLink></li>
      </ul>
      <p className="menu-label">
        Transactions
      </p>
      <ul className="menu-list">
        <li><a href="/as">Payments</a></li>
        <li><a href="/as">Transfers</a></li>
        <li><a href="/as">Balance</a></li>
      </ul>
    </aside>   
  )
}

export default Sidebar
