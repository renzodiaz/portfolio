import React, {Fragment} from 'react'
import Filter from './Filter'
import ProjectList from './List'

class Project extends React.Component {

  render() {
    return (
			<Fragment>
        <Filter />
				<ProjectList />
			</Fragment>
    )
  }

}

export default Project
