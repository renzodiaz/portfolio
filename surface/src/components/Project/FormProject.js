import React, {useState, useEffect} from 'react'
import isEmpty from 'lodash/isEmpty'
import { withFormik, Form } from 'formik'
import { plainAxiosInstance } from '../../utils/Http'
import * as yup from 'yup'

import { Input, Select, MultiSelect, File } from '../Shared/Form'
import TextEditor from '../Shared/TextEditor'

const INITIAL_VALUES = {
  title: '',
  image: null,
  link: '',
  summary: '',
  phone: '',
  all_tags: '',
  client_id: ''
}

const fetchTags = async setTags => {
  await plainAxiosInstance.get('/api/v1/tags')
    .then(res => {
      const {
        data
      } = res
      setTags(data)
    })
    .catch(error => console.log(error))
}

const handleSubmit = async (data, props) => {
  try {
    const isEdit = !isEmpty(props.project)
    if (isEdit) {
      await props.onSubmitAction(props.project.id, data)
    } else {
      await props.onSubmitAction(data)
    }
  } catch(error) {
    console.log(error, "Something went wrong")
  }
}

const FormProject = (props) => {
  const {setFieldValue, isSubmitting, project, values} = props
  const [tags, setTags] = useState([])
  const rteState = project && project.summary ? JSON.parse(project.summary) : [{ type: '', children: [ {text: ''} ] }]
  const [summary, setSummary] = useState(rteState)

  useEffect(()=> {
    fetchTags(setTags)
  }, [])

  function handleChange(event) {
    setFieldValue("image", event.currentTarget.files[0])
  }

  console.log(values.image)

  return(
  <Form encType="multipart/form-data">
      <div className="field">
        <div id="preview-image">
    { 
        project && project.image.url ? <img src={project.image.url} alt={project.name} /> : 
      <img src={values.image ? URL.createObjectURL(values.image) : null} alt={project.name || "new image"} />
    }
        </div>
        <File name="image" onChange={handleChange}/>
      </div>
      <div className="field">
        <label className="label">Title</label>
        <Input 
          name="title"
          placeholder="e.g Web development"
        />
      </div>
      <div className="field">
        <label className="label">Client</label>
        <Select name="client_id" placeholder="Client">
        {
          props.clients &&
          props.clients.map(
            ({id: value, name}) => (
              <option key={`${name}-${value}`} value={value}>
                {name}
              </option>
            )
          )
        }
        </Select>
      </div>
      <div className="field">
        <label className="label">Tags</label>
        <MultiSelect multiple={true} name="all_tags" placeholder="Select tags" onChange={evt => 
          setFieldValue("all_tags", [].slice.call(evt.target.selectedOptions).map(option => option.value))
        }>
    {
        !isEmpty(tags) &&
        tags.map(
        ({id, name}) => (
        <option key={`${name}-${id}`} value={name}>{name}</option>
        )
      )
    }
        </MultiSelect>
      </div>
      <div className="field">
        <label className="label">Link</label>
        <div className="control">
          <Input 
            name="link"
            placeholder= "e.g. https://domain.com"
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Tags</label>
        <div className="control">
        </div>
      </div>
      <div className="field">
      <TextEditor value={summary} onChange={value => {
        setFieldValue("summary", JSON.stringify(value))
        setSummary(value)
      }} />
      </div> 
      <div className="field is-grouped is-grouped-right">
        <p className="control">
          <button type="submit" disabled={isSubmitting} className="button is-primary">Save</button>
        </p>
      </div>
    </Form>
  )
}

const formikConfig = {
  enableReinitialize: true,
  mapPropsToValues: ({project}) => {
    return {...INITIAL_VALUES, ...project}
  },
  handleSubmit: async (values, {props, setSubmitting, resetForm}) => {
    const {
      title,
      image,
      summary,
      link,
      all_tags,
      client_id
    } = values

    const data = {
      title: title,
      link: link,
      summary: summary,
      all_tags: all_tags,
      client_id: client_id
    }

    const formData = new FormData()
    
    Object.entries(data).forEach(
      ([key, value]) => formData.append(key, value)
    )
    formData.append("image", image)

    await handleSubmit(formData, props)
    setSubmitting()
    if(isEmpty(props.project)) {
      resetForm()
      props.history.push("/projects")
    }

  },
  validationSchema: () => {
    return yup.object().shape({
      title: yup
      .string()
      .required("Title is required"),
      link: yup
      .string()
      .required("Website is required")
      .url("Enter a valid URL"),
      summary: yup
      .string(),
			phone: yup
			.string(),
      client_id: yup
      .string()
      .required("Select client")
      .matches(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i, "Select a valid id")
    })
  }
}

export default withFormik(formikConfig)(FormProject)
