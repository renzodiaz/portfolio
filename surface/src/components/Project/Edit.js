import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import isEmpty from 'lodash/isEmpty'
import { fetchProject, updateProject } from '../../actions/projectActions'
import { fetchClients} from '../../actions/clientActions'

import FormProject from './FormProject'

const Edit = ({project, fetchProject, clients, fetchClients, updateProject, match}) => {
  useEffect(()=>{
    fetchProject(match.params.id)
    fetchClients()
  }, [fetchProject, fetchClients, match.params.id])
 
  return (
    isEmpty(project) ? null :
    <div className="columns is-centered">
      <div className="column is-4">
        <h1 className="title is-size-4">Edit project</h1>
        <FormProject clients={clients} project={project} onSubmitAction={updateProject} />
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  project: state.projects.item,
  clients: state.clients.items
})

export default connect(mapStateToProps, {fetchProject, fetchClients, updateProject})(Edit)
