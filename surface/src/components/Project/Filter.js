import React, {Fragment} from 'react'
import { Link } from 'react-router-dom'

const Filter = () => {
  return(
    <Fragment>
      <h1 className="title is-size-4">Projects</h1>
      <nav className="level">
        <div className="level-left">
          <p className="level-item"><strong><small>Show</small></strong>&nbsp;&nbsp;</p>
          <div className="level-item">
            <div className="select is-small">
              <select>
                <option>5</option>
                <option>10</option>
              </select>
            </div>
          </div>
        </div>
        <div className="level-right">
          <p className="level-item">
            <button className="button is-small">
              <span>Export</span>
              <span className="icon"><i className="fas fa-file-excel"></i></span>
            </button>
          </p>
          <p className="level-item">
            <Link to="projects/new" className="button is-success is-small">
              <span>New project</span>
              <span className="icon"><i className="fas fa-plus"></i></span>
            </Link>
          </p>
        </div>
      </nav>
    </Fragment>
  )
}

export default Filter
