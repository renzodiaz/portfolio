import React, { useEffect } from 'react'
import Moment from 'react-moment'
import swal from 'sweetalert'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchProjects, destroyProject} from '../../actions/projectActions'

const ProjectList = (props) => {
  const { fetchProjects, destroyProject, projects } = props

  useEffect(()=> {
    fetchProjects()
  }, [fetchProjects])
  
  return (
    <>
      <table className="table is-fullwidth is-striped is-hoverable">
        <thead>
          <tr>
            <th>Thumb</th>
            <th>Name</th>
            <th>Link</th>
            <th>Client</th>
            <th>Tags</th>
            <th>Date</th>
            <th className="has-text-centered">Actions</th>
          </tr>
        </thead>
        <tbody>
    {
      projects && projects.map((project) => (
				<tr key={`${project.title}-${project.id}`}>
					<td><img src={project.image.thumb.url} alt="my awesome"/></td>
					<td><strong>{project.title}</strong></td>
					<td>
            <a href={project.link} title={project.name} className="button is-small" target="blank" rel="no-opener no-follow">
              <span className="icon">
                <i className="fas fa-external-link-alt"></i>
              </span>
              <span>Website</span>
            </a>
          </td>
					<td>{project.client && project.client.name}</td>
        <td>{
          project.tags && project.tags.map((tag) => ` ${tag.name}`)
        }</td>
					<td><Moment format="LL">{project.created_at}</Moment></td>
					<td>
            <div className="buttons is-centered">
              <Link to={ `projects/edit/${project.id}`} className="button is-small is-info">
                <span>Edit</span>
                <span className="icon is-small">
                  <i className="fas fa-edit"></i>
                </span>
              </Link>
              <button type="button" data-id={project.id} className="button is-small is-danger" onClick={() => {
                  swal({
                    text: `are you sure want to delete ${project.name}`,
                    icon: "warning",
                    dangerMode: true,
                    buttons: true
                  }).then((willDelete) => {
                    if (!willDelete) {
                      swal.close()
                      return;
                    }
                    destroyProject(project.id)
                  }) 
                }
              }>
                <span>Delete</span>
                <span className="icon is-small">
                  <i className="fas fa-times"></i>
                </span>
              </button>
            </div>
					</td>
				</tr>
      )) 
    }
        </tbody>
      </table>
    </>
  )
}

const mapStateToProps = state => ({
  projects: state.projects.items
})
export default connect(mapStateToProps, {fetchProjects, destroyProject})(ProjectList)
