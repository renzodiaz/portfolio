import React, {useEffect} from 'react'
import { connect } from 'react-redux'
import isEmpty from 'lodash/isEmpty'
import FormProject from './FormProject'
import { fetchClients  } from '../../actions/clientActions'
import { createProject } from '../../actions/projectActions'

const New = ({clients, fetchClients, createProject, history}) => {
  
  useEffect(() =>{
    fetchClients()
  }, [fetchClients])

  return (
    isEmpty(clients) ? null :
    <div className="columns is-centered">
      <div className="column is-4">
        <h1 className="title">Create</h1>
        <FormProject clients={clients} onSubmitAction={createProject} history={history}/>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  clients: state.clients.items
})

export default connect(mapStateToProps, {createProject, fetchClients})(New)
