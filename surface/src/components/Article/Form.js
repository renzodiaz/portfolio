import React, { useEffect, useState } from 'react'
import TextEditor from '../Shared/TextEditor'
import * as yup from 'yup'
import { withFormik, Form } from 'formik'
import { Input, Select } from '../Shared/Form'
import { plainAxiosInstance } from '../../utils/Http'

const INITIAL_VALUE = {
  title: "",
  body: "",
  image: null,
  category_id: 1
}

const handleSubmit = (data, props) => {
   
}

const fetchCategories = async (setCategories) => {
  await plainAxiosInstance.get('/api/v1/categories')
    .then(res => {
      const {
        data
      } = res
      setCategories(data)
    })
    .catch(error => console.log(error))
}

const ArticleForm = props => {

  const { setFieldValue, isSubmitting } = props
  const [categories, setCategories] = useState([])
  const [body, setBody] = useState([
    {
      type: 'paragraph',
      children: [
        { text: '' }
      ]
    }
  ])

  useEffect(() => {
    fetchCategories(setCategories)
  }, [])

  return(
    <Form>
      <div className="columns">
        <div className="column is-8">
          <div className="field">
            <Input name="title" className="input is-medium" placeholder="Article title" /> 
          </div>
          <div className="field">
            <TextEditor value={body} onChange={ value => {
              setFieldValue("body", JSON.stringify(value))
              setBody(value)
            }} />
          </div>
        </div>
        <div className="column is-4">
          <div className="field">
            <Select name="category_id" placeholder="Category">
    {
      categories && categories.map(({id: value, name}) => (
          <option key={`${name}-${value}`} value={value}>{name}</option>
      ))
    }
            </Select>
          </div>
          <div className="field is-grouped ">
            <div className="control">
              <button type="submit" disabled={isSubmitting} className="button is-primary">Save</button>
            </div>
            <div className="control">
              <button type="submit" disabled={isSubmitting} className="button is-link">Publish</button>
            </div>
          </div>
        </div>
      </div>
    </Form>
  )

}

const formikConfig = {
  mapPropsToValues: ({ article = {}}) => {
    return {...INITIAL_VALUE, ...article
  }},
  handleSubmit: async (values, {props, resetForm, setSubmitting}) => {

    const {
      title,
      body,
      category_id,
      status
    } = values

    const data = {
      title: title,
      body: body,
      category_id: category_id,
      status: status
    }

    await handleSubmit(data, props)

    setSubmitting()
  },
  validationSchema: () => {
    return yup.object().shape({
      title: yup
      .string()
      .required("Title is required"),
      body: yup
      .string()
      .required("Article content is required"),
      category_id: yup
      .string()
      .required("Select a category")
    })
  }
}

export default withFormik(formikConfig)(ArticleForm)
