import React, { useEffect } from 'react'
import Moment from 'react-moment'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchArticles } from '../../actions/articleActions'

const List = (props) => {

  const { fetchArticles, articles } = props

  useEffect(() => {
    fetchArticles()
  }, [fetchArticles])

  return(
    <>
      <div className="table-container">
        <table className="table is-fullwidth is-striped is-hoverable">
          <thead>
            <tr>
              <th>Title</th>
              <th>Author</th>
              <th>Categories</th>
              <th>Tags</th>
              <th>Status</th>
              <th>Date</th>
              <th>Action</th>
            </tr> 
          </thead>
          <tbody>
    {
      articles && articles.map(article => (
            <tr key={article.id}>
              <td><strong>{article.title}</strong></td>
              <td>Renzo Diaz</td>
              <td>Web Development</td>
              <td>#rails, #tutorial</td>
              <td>{article.status}</td>
              <td><Moment format="LL">{article.created_at}</Moment></td>
              <td>
                <Link to={`/clients/edit/${article.id}`} className="button is-small is-info">
                  <span>Edit</span>
                  <span className="icon is-small">
                    <i className="fa fa-edit"></i>
                  </span>
                </Link> 
              </td>
            </tr> 
      )) 
    }
          </tbody>
        </table> 
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  articles: state.articles.items
})
export default connect(mapStateToProps, {fetchArticles})(List)
