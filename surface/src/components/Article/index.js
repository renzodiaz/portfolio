import React from 'react'
import { Link, useRouteMatch} from 'react-router-dom'
import List from './List'

const Article = (props) => {
  
  const { url } = useRouteMatch()

  return (
    <>
      <h1 className="title is-size-4">Articles</h1>
      <Link to={ `${ url }/new` } className="button is-small is-success">
        <span>New Article</span> 
        <span className="icon is-small">
          <i className="fa fa-plus"></i>
        </span>
      </Link>
      <List />
    </>
  )
}

export default Article
