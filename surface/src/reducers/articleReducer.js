import {
  FETCH_ARTICLES,
  FETCH_ARTICLES_SUCCESS,
  FETCH_ARTICLE,
  FETCH_ARTICLE_SUCCESS,
  REQUEST_ARTICLE_FAILURE
} from '../actions/types'

const INITIAL_STATE = {
  items: [],
  item: {},
  isLoading: false,
  error: null
}

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case FETCH_ARTICLES:
    case FETCH_ARTICLE:
      return {
        ...state,
        isLoading: true,
        error: false
      }
    case FETCH_ARTICLES_SUCCESS:
      return {
        items: [...action.payload],
        isLoading: false,
        error: false
      }
    case FETCH_ARTICLE_SUCCESS:
      return {
        item: Object.assign({}, action.payload),
        isLoading: false,
        error: false
      }
    case REQUEST_ARTICLE_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      }
    default:
      return state
  }
}
