import { 
  FETCH_PROJECTS,
  FETCH_PROJECTS_SUCCESS,
  FETCH_PROJECT,
  FETCH_PROJECT_SUCCESS,
  CREATE_PROJECT,
  CREATE_PROJECT_SUCCESS,
  UPDATE_PROJECT,
  UPDATE_PROJECT_SUCCESS,
  DESTROY_PROJECT,
  DESTROY_PROJECT_SUCCESS,
  REQUEST_PROJECT_FAILURE
} from '../actions/types'

const initialState = {
  items: [],
  item: {},
  isLoading: false,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PROJECTS:
    case FETCH_PROJECT:
    case CREATE_PROJECT:
    case UPDATE_PROJECT:
    case DESTROY_PROJECT:
      return {
        ...state,
        isLoading: true,
        error: null
      }
    case FETCH_PROJECTS_SUCCESS:
      return {
        items: [...action.payload],
        isLoading: false,
        error: null
      }
    case FETCH_PROJECT_SUCCESS:
      return {
        ...state,
        item: Object.assign({}, action.payload),
        isLoading: false,
        error: null
      }
    case CREATE_PROJECT_SUCCESS:
      return {
        ...state,
        item: action.payload
      }
    case UPDATE_PROJECT_SUCCESS:
      return {
        ...state,
        item: action.payload
      }
		case DESTROY_PROJECT_SUCCESS:
			return {
				...state,
				items: state.items.filter(item => item.id !== action.payload)
			}
    case REQUEST_PROJECT_FAILURE:
      return {
        isLoading: false,
        error: action.payload
      }
    default:
      return state;
  }

} 
