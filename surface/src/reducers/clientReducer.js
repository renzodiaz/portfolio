import { 
  REQUEST_CLIENT_FAILURE,
  FETCH_CLIENTS,
  FETCH_CLIENTS_SUCCESS,
  FETCH_CLIENT,
  FETCH_CLIENT_SUCCESS,
  CREATE_CLIENT,
  CREATE_CLIENT_SUCCESS,
  UPDATE_CLIENT,
  UPDATE_CLIENT_SUCCESS,
  DESTROY_CLIENT,
  DESTROY_CLIENT_SUCCESS
} from '../actions/types'

const initialState = {
  items: [],
  item: {},
  isLoading: false,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CLIENTS:
    case FETCH_CLIENT:
    case CREATE_CLIENT:
    case UPDATE_CLIENT:
    case DESTROY_CLIENT:
      return {
        ...state,
        isLoading: true,
        error: null
      }
    case FETCH_CLIENTS_SUCCESS:
      return {
        items: [...action.payload],
        isLoading: false,
        error: null
      }
    case FETCH_CLIENT_SUCCESS:
      return {
        ...state,
        item: Object.assign({}, action.payload),
        isLoading: false,
        error: null
      }
    case CREATE_CLIENT_SUCCESS:
      return {
        ...state,
        item: action.payload,
        isLoading: false,
        error: null
      }
    case UPDATE_CLIENT_SUCCESS:
      return {
        ...state,
        item: action.payload,
        isLoading: false,
        error: null
      }
    case DESTROY_CLIENT_SUCCESS:
      return {
        ...state,
        items: state.items.filter(item => item.id !== action.payload),
        isLoading: false,
        error: null
      }
    case REQUEST_CLIENT_FAILURE:
      return {
        isLoading: false,
        error: action.payload
      }
    default:
      return state;
  }
} 
