import {
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  USER_SIGNED_IN,
  USER_SIGNED_IN_SUCCESS,
  USER_LOGOUT,
  USER_LOGOUT_SUCCESS,
  REQUEST_FAILURE
} from '../actions/types'

const INITIAL_STATE = {
  isAuthenticated: false,
  isLoading: false,
  error: null,
  currentUser: {}
}

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case USER_LOGIN:
      return {
        ...state,
        isLoading: true,
        isAuthenticated: false,
        error: null
      }
    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: true,
        error: null,
        currentUser: Object.assign({}, action.payload)
      }
    case USER_SIGNED_IN:
      return {
        ...state,
        isLoading: true,
        isAuthenticated: false,
        error: null
      }
    case USER_SIGNED_IN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: action.payload,
        error: null
      }
    case USER_LOGOUT:
      return {
        ...state,
        isLoading: true,
        isAuthenticated: true,
        error: null
      }
    case USER_LOGOUT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: false,
        error: null
      }
    case REQUEST_FAILURE:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: false,
        error: action.payload
      }
    default:
      return state
  }
}
