import { 
  FETCH_TAGS, 
  FETCH_TAG,
  TAG_CLEAR,
  NEW_TAG,
  UPDATE_TAG,
  DESTROY_TAG 
} from '../actions/types'

const initialState = {
  items: [],
  item: {}
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TAGS:
      return {
        ...state,
        items: action.payload
      }
    case FETCH_TAG:
      return {
        ...state,
        item: Object.assign({}, action.payload)
      }
    case TAG_CLEAR:
      return {
        ...state,
        item: {}
      }
    case NEW_TAG:
      return {
        ...state,
        item: action.payload
      }
    case UPDATE_TAG:
      return {
        ...state,
        item: action.payload
      }
		case DESTROY_TAG:
			return {
				...state,
				items: state.items.filter(item => item.id !== action.payload)
			}
    default:
      return state;
  }

} 
