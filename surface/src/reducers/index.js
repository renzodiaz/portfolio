import { combineReducers } from 'redux'
import clientReducer from './clientReducer'
import projectReducer from './projectReducer'
import tagReducer from './tagReducer'
import articleReducer from './articleReducer'
import userReducer from './userReducer'

export default combineReducers({
  clients: clientReducer,
  projects: projectReducer,
  articles: articleReducer,
  tags: tagReducer,
  user: userReducer
})
