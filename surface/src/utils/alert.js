import swal from 'sweetalert'
const sweetAlert = (type, message) => {
  swal({
    title: message,
    icon: type
  })
}

export { sweetAlert }
