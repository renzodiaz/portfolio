import { plainAxiosInstance, secureAxiosInstance } from './Http'
import { sweetAlert } from './alert'

const signedInFailed = (err) => {
  const error = (err.response && err.response.data && err.response.data.error) || ''
  console.log(err)
  if (error) {
    console.log(error)
  }
  
  deleteSession()
}

const deleteSession = () => {
  delete localStorage.csrf
  delete localStorage.signedIn
}

// Login params (email, password)
const loginUser = (email, password) => {
  return plainAxiosInstance.post('/signin', { email: email, password: password })
    .then(res => {
      if(!res.data.csrf) {
        signedInFailed(res)
        return
      }
      localStorage.csrf = res.data.csrf
      localStorage.signedIn = true
    })
    .catch(err => signedInFailed(err))
}

const logoutUser = () => {
  return secureAxiosInstance.delete('/logout')
    .then(res => {
      deleteSession()
      window.location.href = "/"
    })
    .catch(err => console.log("error", err))
}

const checkSignedIn = () => !!localStorage.signedIn

export { loginUser, logoutUser, checkSignedIn }
