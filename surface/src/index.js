import "babel-polyfill"
import React from 'react'
import renderDOM from 'react-dom'

import Root from './Root'

renderDOM.render(
  <Root/>,
  document.getElementById('app')
)
