import {
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
  USER_LOGOUT_SUCCESS,
  USER_SIGNED_IN,
  USER_SIGNED_IN_SUCCESS,
  REQUEST_FAILURE
} from './types'

export const userLogin = (formData) => ({
  type: USER_LOGIN,
  payload: formData
})

export const userLoginSuccess = (response) => ({
  type: USER_LOGIN_SUCCESS,
  payload: response
})

export const userSignedIn = () => ({
  type: USER_SIGNED_IN
})

export const userSignedInSuccess = (signedIn) => ({
  type: USER_SIGNED_IN_SUCCESS,
  payload: signedIn
})

export const userLogout = () => ({
  type: USER_LOGOUT
})

export const userLogoutSuccess = () => ({
  type: USER_LOGOUT_SUCCESS,
  payload: {}
})

export const requestUserFailure = (message) => ({
  type: REQUEST_FAILURE,
  payload: message
})
