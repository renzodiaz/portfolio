import { 
  FETCH_PROJECTS,
  FETCH_PROJECTS_SUCCESS,
  FETCH_PROJECT,
  FETCH_PROJECT_SUCCESS,
  CREATE_PROJECT,
  CREATE_PROJECT_SUCCESS,
  UPDATE_PROJECT,
  UPDATE_PROJECT_SUCCESS,
  DESTROY_PROJECT,
  DESTROY_PROJECT_SUCCESS,
  REQUEST_PROJECT_FAILURE
} from './types'

export const fetchProjects = () => ({
  type: FETCH_PROJECTS
})

export const fetchProjectsSuccess = (projects) => ({
  type: FETCH_PROJECTS_SUCCESS,
  payload: projects
})

export const fetchProject = (project_id) => ({
  type: FETCH_PROJECT,
  payload: project_id
})

export const fetchProjectSuccess = (project) => ({
  type: FETCH_PROJECT_SUCCESS,
  payload: project
})

export const createProject = (form_data) => ({
  type: CREATE_PROJECT,
  payload: form_data
})

export const createProjectSuccess = (project) => ({
  type: CREATE_PROJECT_SUCCESS,
  payload: project
})

export const updateProject = (project_id, form_data) => ({
  type: UPDATE_PROJECT,
  payload: { 
    id: project_id,
    data: form_data
  }
})

export const updateProjectSuccess = (project) => ({
  type: UPDATE_PROJECT_SUCCESS,
  payload: project
})

export const destroyProject = (project_id) => ({
  type: DESTROY_PROJECT,
  payload: project_id
})

export const destroyProjectSuccess = (project_id) => ({
  type: DESTROY_PROJECT_SUCCESS,
  payload: project_id
})

export const requestProjectFailure = (message) => ({
  type: REQUEST_PROJECT_FAILURE,
  payload: message
})
