import { 
  FETCH_TAGS, 
  FETCH_TAG,
  TAG_CLEAR,
  NEW_TAG, 
  UPDATE_TAG, 
  DESTROY_TAG 
} from './types'
import { plainAxiosInstance, secureAxiosInstance } from '../utils/Http'

export const fetchTags = () => dispatch => {
  plainAxiosInstance.get('/api/v1/tags')
    .then(res => {
      dispatch({
        type: FETCH_TAGS,
        payload: res.data
      })
    })
    .catch(error => console.log("error", error))
}

export const fetchTag = (tagID) => dispatch => {
	secureAxiosInstance.get(`/api/v1/tags/${tagID}`)
	.then(res => {
		dispatch({
			type: FETCH_TAG,
			payload: res.data
		})
	})
	.catch(error => console.log("error", error))
}

export const tagClear = () => ({
  type: TAG_CLEAR
})

export const createTag = (tagData) => dispatch => {
  secureAxiosInstance.post('/api/v1/tags', tagData)
    .then(res => {
      dispatch({
        type: NEW_TAG,
        payload: res.data
      })
    })
    .catch(error => console.log("error", error))
}

export const updateTag = (tagData) => dispatch => {
  const { id, ...rest } = tagData
  secureAxiosInstance.patch(`/api/v1/tags/${id}`, rest)
    .then(res => {
      dispatch({
        type: UPDATE_TAG,
        payload: res.data
      })
      console.log("success", "Tag edited")
    })
    .catch(error => console.log(error.error))
}

export const destroyTag = (tagID) => dispatch => {
  let confirm = window.confirm("Are you sure you want delete this item")
  if (!confirm)
    return

	secureAxiosInstance.delete(`/api/v1/tags/${tagID}`)
	.then(res => {
		dispatch({
			type: DESTROY_TAG,
			payload: tagID
		})
	})
	.catch(error => console.log(error))
}
