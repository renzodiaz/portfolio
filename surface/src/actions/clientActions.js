import { 
  FETCH_CLIENTS, 
  FETCH_CLIENTS_SUCCESS, 
  FETCH_CLIENT,
  FETCH_CLIENT_SUCCESS,
  CREATE_CLIENT,
  CREATE_CLIENT_SUCCESS,
  UPDATE_CLIENT, 
  UPDATE_CLIENT_SUCCESS, 
  DESTROY_CLIENT,
  DESTROY_CLIENT_SUCCESS,
  REQUEST_CLIENT_FAILURE
} from './types'

export const fetchClients = () => ({
  type: FETCH_CLIENTS
})

export const fetchClientsSuccess = (clients) => ({
  type: FETCH_CLIENTS_SUCCESS,
  payload: clients
})

export const fetchClient = (client_id) => ({
  type: FETCH_CLIENT,
  payload: client_id
})

export const fetchClientSuccess = (client) => ({
  type: FETCH_CLIENT_SUCCESS,
  payload: client
})

export const createClient = (form_data) => ({
  type: CREATE_CLIENT,
  payload: form_data
})

export const createClientSuccess = (client) => ({
  type: CREATE_CLIENT_SUCCESS,
  payload: client
})

export const updateClient = (form_data) => ({
  type: UPDATE_CLIENT,
  payload: form_data
})

export const updateClientSuccess = (client) => ({
  type: UPDATE_CLIENT_SUCCESS,
  payload: client
})

export const destroyClient = (client_id) => ({
  type: DESTROY_CLIENT,
  payload: client_id
})

export const destroyClientSuccess = (clients) => ({
  type: DESTROY_CLIENT_SUCCESS,
  payload: clients
})

export const requestClientFailure = (message) => ({
  type: REQUEST_CLIENT_FAILURE,
  payload: message
}) 
