import {
  FETCH_ARTICLES,
  FETCH_ARTICLES_SUCCESS,
  FETCH_ARTICLE,
  FETCH_ARTICLE_SUCCESS,
  CREATE_ARTICLE,
  CREATE_ARTICLE_SUCCESS,
  UPDATE_ARTICLE,
  UPDATE_ARTICLE_SUCCESS,
  DESTROY_ARTICLE,
  DESTROY_ARTICLE_SUCCESS,
  REQUEST_ARTICLE_FAILURE
} from './types'

export const fetchArticles = () => ({
  type: FETCH_ARTICLES
})

export const fetchArticlesSuccess = (articles) => ({
  type: FETCH_ARTICLES_SUCCESS,
  payload: articles
})

export const requestArticleFailure = (message) => ({
  type: REQUEST_ARTICLE_FAILURE,
  payload: message
})
