class CreateTaggings < ActiveRecord::Migration[6.0]
  def change
    create_table :taggings, id: :uuid do |t|
      t.belongs_to :project, type: :uuid, null: false, foreign_key: true
      t.belongs_to :tag, type: :uuid, null: false, foreign_key: true

      t.timestamps
    end
  end
end
