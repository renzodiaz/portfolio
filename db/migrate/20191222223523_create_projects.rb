# frozen_string_literal: true

class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects, id: :uuid do |t|
      t.string :title
      t.string :image
      t.string :link
      t.text :summary
      t.belongs_to :client, type: :uuid

      t.timestamps
    end
  end
end
