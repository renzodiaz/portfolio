class CreateArticles < ActiveRecord::Migration[6.0]
  def change
    create_table :articles, id: :uuid do |t|
      t.string :title
      t.text :body
      t.string :image
      t.integer :status, null: false, default: 1
      t.references :user, type: :uuid, null: false, foreign_key: true

      t.timestamps
    end
  end
end
