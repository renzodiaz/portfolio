class CreateArticleCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :article_categories, id: :uuid do |t|
      t.belongs_to :article, type: :uuid, null: false, foreign_key: true
      t.belongs_to :category, type: :uuid, null: false, foreign_key: true

      t.timestamps
    end
  end
end
