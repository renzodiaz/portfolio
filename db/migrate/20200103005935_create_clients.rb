class CreateClients < ActiveRecord::Migration[6.0]
  def change
    create_table :clients, id: :uuid do |t|
      t.string :name
      t.string :company
      t.string :website
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end
