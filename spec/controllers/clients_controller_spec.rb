require 'rails_helper'
require 'rspec/collection_matchers'
require 'json'

RSpec.describe Api::V1::ClientsController, type: :controller do

  describe 'GET #index' do
    context 'When data exists' do
      it 'return 10 items' do
        get :index, params: {page: 1, per_page: 10}, format: :json
        expect(response.body).to have_at_most(10).items
      end
      it 'responds 200 (OK)' do
        get :index
        expect(response).to have_http_status(200)
      end
    end
  end

end
