require 'rails_helper'

RSpec.describe Client, type: :model do
  subject {
    described_class.new(name: "Anything", company: "Anything",
                        email: "test@test.com", website: "https://website.com", phone: "5555555555")
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid w/o name" do
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it "is not valid w/o company" do
    subject.company = nil
    expect(subject).to_not be_valid
  end

  it "is not valid w/o email" do
    subject.email = nil
    expect(subject).to_not be_valid
  end

  it "is not valid w/o website" do
    subject.website = nil
    expect(subject).to_not be_valid
  end

end
