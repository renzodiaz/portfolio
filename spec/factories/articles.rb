FactoryBot.define do
  factory :article do
    title { "MyString" }
    body { "MyText" }
    status { 1 }
    image { "MyString" }
    user { nil }
  end
end
