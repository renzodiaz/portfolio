FactoryBot.define do
  factory :client do
    name { "MyString" }
    website { "MyString" }
    email { "MyString" }
    phone { "MyString" }
  end
end
