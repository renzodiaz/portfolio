# frozen_string_literal: true
JWTSessions.access_exp_time = 3600
JWTSessions.algorithm = "HS256"
JWTSessions.encryption_key = Rails.application.credentials[:secret_key_base]
