# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :projects
      resources :clients
      resources :articles
      resources :tags
      resources :categories, only: :index
      get 'users/:username', controller: :users, action: :show
    end
  end

  post 'refresh', controller: :refresh, action: :create
  post 'signin', controller: :signin, action: :create
  delete 'logout', controller: :signin, action: :destroy

  get '*patch', to: "application#fallback_index_html", constraints: ->(request) do
    !request.xhr? && request.format.html?
  end
end
